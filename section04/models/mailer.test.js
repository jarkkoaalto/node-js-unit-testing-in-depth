const chai = require('chai');
const expect = chai.expect;
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
chai.use(sinonChai);
const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
const rewire = require('rewire');

var mailer = rewire('./mailer');
var sandbox = sinon.sandbox.create();

describe('users', () => {
    let emailStub;

    beforeEach(() => {
        emailStub = sandbox.stub().resolves('done');
        mailer.__set__('sendEmail', emailStub);
    });

    afterEach(() => {
        sandbox.restore();
        mailer = rewire('./mailer');
    });

    context('sendWelcomeEmail', () => {
        it('should check for an email and name', async () => {
            await expect(mailer.sendWelcomeEmail()).to.eventually.be.rejectedWith('Invalid input');
            await expect(mailer.sendWelcomeEmail('foo@bar.com')).to.eventually.be.rejectedWith('Invalid input');
            await expect(mailer.sendWelcomeEmail(null, 'foo')).to.eventually.be.rejectedWith('Invalid input');
        });

        it('should call sendEmail with email and message', async () => {
            await mailer.sendWelcomeEmail('foo@bar.com', 'foo');

            expect(emailStub).to.have.been.calledWith('foo@bar.com', 'Dear foo, welcome to our family!');
        });
    });

    context('sendPasswordResetEmail', () => {
        it('should check for an email and name', async () => {
            await expect(mailer.sendPasswordResetEmail()).to.eventually.be.rejectedWith('Invalid input');
            await expect(mailer.sendPasswordResetEmail(null)).to.eventually.be.rejectedWith('Invalid input');
        });

        it('should call sendEmail with email and message', async () => {
            await mailer.sendPasswordResetEmail('foo@bar.com', 'foo');

            expect(emailStub).to.have.been.calledWith('foo@bar.com', 'Please click http://some_link to reset your password.');
        });
    });

    context('sendEmail', () => {
        let sendEmail;

        beforeEach(() => {
            mailer = rewire('./mailer');
            sendEmail = mailer.__get__('sendEmail');
        });

        it('should check for an email and body', async () => {
            await expect(sendEmail()).to.eventually.be.rejectedWith('Invalid input');
            await expect(sendEmail('foo@bar.com')).to.eventually.be.rejectedWith('Invalid input');
            await expect(sendEmail(null, 'foo')).to.eventually.be.rejectedWith('Invalid input');
        });

        it('should call sendEmail with email and message', async () => {
            let result = await sendEmail('foo@bar.com', 'some message');

            expect(result).to.equal('Email sent');
        });
    });
})