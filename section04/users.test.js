const chai = require('chai');
const expect = chai.expect;
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
chai.use(sinonChai);


const chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);

var mongoose = require('mongoose');

var users = require('./users');
var User = require('./models/user');
var mailer = require('./mailer');

var sandbox = sinon.sandbox.create();

describe('users', () => {
    let saveStub;
    let findStub;
    let sampleUserSaveStub;
    let mailerStub;
    // let sampleArgs;
    // let sampleUser;

    beforeEach(() => {
        //note about (mongoose.MODEL, 'save') stub NOT WORKING, you have to use User.prototype
        sampleArgs = {
            name: 'foo',
            email: 'foo@bar.com'
        };
        sampleUserSaveStub = sandbox.stub().resolves(sampleArgs);
        sampleUser = {
            id: 123,
            name: 'foo',
            email: 'foo@bar.com',
            save: sampleUserSaveStub
        };

        saveStub = sandbox.stub(User.prototype, 'save').resolves(sampleUser);
        findStub = sandbox.stub(mongoose.Model, 'findById').resolves(sampleUser);
        mailerStub = sandbox.stub(mailer, 'sendWelcomeEmail').resolves('fake_email_resolve');
        // spy = sandbox.spy(User);
    });

    afterEach(() => {
        sandbox.restore();
      
    });

    //callback
    context('get', () => {
        //callbacks
        it('should check for an id', (done) => {
            users.get(null, function (err, result) {
                expect(err).to.be.instanceOf(Error);
                expect(err.message).to.equal('Invalid user id');
                done();
            });
        });

        //callback + stub + yields
        it('should call findUserById and return result', (done) => {
            sandbox.restore();
            let stub = sandbox.stub(mongoose.Model, 'findById').yields(null, {
                name: 'foo'
            });

            users.get(123, function (err, result) {
                expect(err).to.be.null;
                expect(stub).to.have.been.calledWith(123);
                expect(result).to.be.a('object');
                expect(result).to.have.property('name').to.equal('foo');

                stub.restore();
                done();
            });
        });

        // //callback + stub + yields + error check
        it('should return error if there is one', (done) => {
            sandbox.restore();

            let stub = sandbox.stub(mongoose.Model, 'findById').yields(new Error('fake'));

            users.get(123, function (err, result) {
                expect(result).to.not.exist;
                expect(err).to.not.be.null;
                expect(stub).to.have.been.calledWith(123);
                expect(err).to.be.instanceOf(Error);
                expect(err.message).to.equal('fake');

                stub.restore();
                done();
            });
        });
    });

    //promise
    context('delete', () => {
        it('should check for an id using return', () => {
            return users.delete().then((result) => {
                throw new Error('Unexpected success');
            }).catch((err) => {
                expect(err).to.be.instanceOf(Error);
                expect(err.message).to.equal('Invalid id');
            });
        });
    
    
        //promise return
        it('should check for an id with return eventually', () => {
            return expect(users.delete()).to.eventually.be.rejectedWith('Invalid id');
        });
   

        //await eventually
        it('should check for an id with await eventually', async () => {
            await expect(users.delete()).to.eventually.be.rejectedWith('Invalid id');
        });

        // stub
        it('should call User.remove', async () => {
            let stub = sandbox.stub(mongoose.Model, 'remove').resolves('fake_remove_resolve');
            let result = await users.delete(123);

            expect(result).to.equal('fake_remove_resolve');
            expect(stub).to.have.been.calledWith({
                _id: 123
            });

            stub.restore();
        });
    });
    

    // await + eventually + validation example check
    context('create', () => {
        let spy;
        let stub;

        beforeEach(() => {
            // note about (mongoose.MODEL, 'save') stub NOT WORKING, you have to use User.prototype
            // spy = sandbox.spy(User);
        });

        it('should check for an email and name', async () => {
            await expect(users.create()).to.eventually.be.rejectedWith('Invalid arguments');
            await expect(users.create({
                id: 123
            })).to.eventually.be.rejectedWith('Invalid arguments');
            await expect(users.create({
                email: 'foo@bar.com'
            })).to.eventually.be.rejectedWith('Invalid arguments');
            await expect(users.create({
                name: 'foo'
            })).to.eventually.be.rejectedWith('Invalid arguments');
        });

        it('should call user.save', async () => {
            result = await users.create(sampleArgs);

            expect(saveStub).to.have.been.called;
            expect(mailerStub).to.have.been.calledWith(sampleUser.email, sampleUser.name);
            expect(result).to.be.a('object');
            expect(result).to.have.property('message').to.equal('User created');
            expect(result).to.have.property('userId').to.equal(123);
            // console.log(result);
        });
   

        it('should reject if there is an error', async () => {
            sandbox.restore();

            saveStub = sandbox.stub(User.prototype, 'save').rejects(new Error('fake_save_reject'));

            await expect(users.create(sampleArgs)).to.eventually.be.rejectedWith('fake_save_reject');
        });
    });

    context('update', () => {
        it('should find user by id', async () => {
            await users.update(123, {
                age: 35
            });
            expect(findStub).to.have.been.calledWith(123);
        });

        it('should call user.save', async () => {
            await users.update(123, {
                age: 35
            });
            expect(sampleUserSaveStub).to.have.been.calledOnce;
        });

        it('should reject if there is an error', async () => {
            sandbox.restore();

            findStub = sandbox.stub(mongoose.Model, 'findById').rejects(new Error('fake_save_reject'));

            await expect(users.update(sampleArgs)).to.eventually.be.rejectedWith('fake_save_reject');
        });
    });

    context('resetPassword', () => {
        let sendPasswordResetEmailStub;

        beforeEach(() => {
            sendPasswordResetEmailStub = sandbox.stub(mailer, 'sendPasswordResetEmail').resolves('fake');
        });

        it('should check for an email', async () => {
            await expect(users.resetPassword()).to.eventually.be.rejectedWith('Invalid email');
        });

        it('should check for an email', async () => {
            await users.resetPassword('foo@bar.com');

            expect(sendPasswordResetEmailStub).to.have.been.calledWith('foo@bar.com');
        });
    });
    
})