const assert = require('assert');

describe('file to be tested', ()=>{
    context('function to be tested', () =>{
        it('should do something', () =>{
            assert.equal(1,1)
        });

        it ('Should do something else',()=>{
            assert.deepEqual({name: 'joe'},{name:'joe'});
        });

        // pending test, This is example help you planning test
        // cases first even if program development is not 
        // reach end of development cycle.
        it ('this is pending test');
    });
});