# Node-js-Unit-Testing-In-Depth

This is a focused, in-depth course about unit testing, where we will look at lots of code, and learn how to test it. We will go through everything from the simplest JavaScript function to ES6 classes &amp; private code testing. We will also learn how to test your code coverage, so you always know where you stand.


# Node addons

##### Mocha

Mocha is a feature-rich JavaScript test framework running on Node.js and in the browser, making asynchronous testing simple and fun. 
https://mochajs.org/

##### chai

hai is a BDD / TDD assertion library for node and the browser that can be delightfully paired with any javascript testing framework.

https://www.chaijs.com/

##### sinonjs

Standalone test spies, stuns and mocks for JavaScript

https://sinonjs.org/

OR

https://www.chaijs.com/plugins/sinon-chai/

##### rewire

Rewire adds a special setter and getter to modules so you can modify their behaviour for better unit testing. You may

    inject mocks for other modules or globals like process
    inspect private variables
    override variables within the module.


https://github.com/jhnns/rewire

###### Node SuperTest

The motivation with this module is to provide a high-level abstraction for testing HTTP, while still allowing you to drop down to the lower-level API provided by superagent.

https://github.com/visionmedia/supertest

Install

npm install supertest --save-dev

###### Node express

Express provides a thin layer of fundamental web application features, without obscuring Node.js features that you know and love.

install

https://expressjs.com/

 npm install express --save-dev

##### Instanbuljs / NYC

Istanbul instruments your ES5 and ES2015+ JavaScript code with line counters, so that you can track how well your unit-tests exercise your codebase.


https://github.com/istanbuljs/nyc

or 

https://istanbul.js.org/

install:

npm i nyc --save-dev

or

 npm install --save-dev nyc



##### Test coverage warnings

write allways cood test. Example below used useless function and parameter

 ![alt text][example]

[example]: userlessfunctionandtest.png "invisible.js test useless function"