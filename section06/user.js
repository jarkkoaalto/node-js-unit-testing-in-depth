exports.getFullAddress = function(user){
   // return '100 king, London, ON, 1111' // pass .This is not good testing method
   if(!user.street){
       return "Invald user";
   }
    return `${user.street},${user.city}, ${user.province}, ${user.postal}`;
}